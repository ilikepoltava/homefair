import * as types from '@/store/mutation-types'

export const state = () => ({

  // Step 1
  address: '',
  unit_use_identity: null,
  homesize: null,

  garage: false,
  balcony: false,
  bathroom: false,
  kitchen: false,

  loading: false,

  // Step 2
  result: null,

  form: null,

  feedback: null

})

export const actions = {

  async getValueProposition({ state, commit }) {
    commit(`${types.SAVE_SEARCH_KEY}`, { key: 'loading', value: true })

    const { Status, Result, Prediction_id } = await this.$axios.$put('/Get_Value_Proposition', {
      unit_use_identity: state.unit_use_identity,
      homesize: state.homesize,
      garage: state.garage,
      balcony: state.balcony,
      bathroom: state.bathroom,
      kitchen: state.kitchen
    })

    if (Status) {
      await commit(`${types.SAVE_SEARCH_KEY}`, {
        key: 'result',
        value: {
          ...Result,
          prediction_id: Prediction_id
        }
      })

      console.log(state.result);

      this.$router.push({ name: 'sell-info' })
    } else
      $nuxt.$snotify.error('Something went wrong, please try again later', 'Error')

    commit(`${types.SAVE_SEARCH_KEY}`, { key: 'loading', value: false })

  },

  async payCheck({ commit, state }, token) {
    commit(`${types.SAVE_SEARCH_KEY}`, { key: 'loading', value: true })

    const { Status } = await this.$axios.$put('/Get_Product', {
      stripe_card_token: token,
      prediction_id: state.result.prediction_id,
      send_email_to: state.form.email
    })

    if (Status) {
      this.$router.push({ name: 'sell-thanks' })
      $nuxt.$snotify.success('Payment successful', 'Success')
    } else
      $nuxt.$snotify.error('Something went wrong, please try again later', 'Error')

    commit(`${types.SAVE_SEARCH_KEY}`, { key: 'loading', value: false })
  },

  async subscribe({ state }, email) {
    const { Status } = await this.$axios.$post('/Subscribe', { name: '', email })

    if (Status) $nuxt.$snotify.success('Thanks for subscribing!', 'Success')
    else $nuxt.$snotify.error('Something went wrong, please try again later', 'Error')
  },

  async makeFeedback({ state }, feedback) {
    const { Status } = await this.$axios.$put('/Get_Customer_Feedback', { ...feedback, prediction_id: state.result.prediction_id })

    if (Status) $nuxt.$snotify.success('Thanks for feedback!', 'Success')
    else $nuxt.$snotify.error('Something went wrong, please try again later', 'Error')
  }

}

export const mutations = {
  [types.SAVE_SEARCH_KEY](state, { key, value }) {
    state[key] = value
  }
}